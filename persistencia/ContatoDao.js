function ContatoDao(connection) {
	this._connection = connection;
}

ContatoDao.prototype.novo = function(cadastro, callback) {
	this._connection.query('INSERT INTO contatos SET ?', cadastro, callback)
}
ContatoDao.prototype.altera = function(cadastro, callback) {
	this._connection.query('UPDATE contatos SET `status` = ? where id = ?', [cadastro.status, cadastro.id], callback)
}
ContatoDao.prototype.remove = function(cadastro, callback) {
	this._connection.query('UPDATE contatos SET `status` = ? where id = ?', [cadastro.status, cadastro.id], callback)
}
ContatoDao.prototype.lista = function(callback) {
	this._connection.query('select * from contatos', callback)
}
ContatoDao.prototype.exibeItem = function(consulta, callback) {
	this._connection.query('select * from contatos where id = ?', [consulta.id], callback)
}

module.exports = function (connection) {
    return ContatoDao;
}