var mysql = require('mysql');

function createDBConnection() {
	return mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: 'csnohpg',
		database: 'contatos'
	});
}

module.exports = function () {
	return createDBConnection;
}