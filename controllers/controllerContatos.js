module.exports = function(app) {

	//Lista os contatos;
	app.get('/contact', function(req, res){
		console.log("=================")
		console.log("LISTANDO CONTATOS");
		console.log("=================")

		var connection = app.persistencia.connectionFactory();
		var contatoDao = new app.persistencia.ContatoDao(connection);

		contatoDao.lista(function(erro, result){
			if (erro) {
				console.log("*** Não foi listado ***");
				res.status(500).send(erro);
				return;
			} else {
				console.log("Listado com sucesso");
				res.status(201).json(result);
			}
		});
		connection.end();
	})

	//Retorna as informacoes específicas do contato;
	app.get('/contact/:id', function(req, res){
		var id = req.params.id; //pega o id do controller acima (:id)
		var consulta = {};
		consulta.id = id;
		console.log('==================');
		console.log("EXIBINDO DADOS DO CONTATO . [" + id + "]");
		console.log('==================');

		var connection = app.persistencia.connectionFactory();
		var contatoDao = new app.persistencia.ContatoDao(connection);

		contatoDao.exibeItem(consulta, function(erro, result){
			if (erro) {
				console.log("*** Não foi possível consultar o item solicitado. ***")
				res.status(500).send(erro);
				return;
			} else {

				res.status(200).json(result);
			}
		});
		connection.end();
	});

	//Cria um novo contato, é necessário enviar "email" e "name";
	app.post('/contact', function(req, res){
		console.log("=================")
		console.log("CADASTRANDO NOVO CONTATO");
		console.log("=================")

		/* inicia a verificação de erros */
		req.assert("nome","Nome obrigatório.").notEmpty();
		req.assert("email", "E-mail obrigatório.").notEmpty().isEmail();

		var erros = req.validationErrors();
		if (erros) {
			console.log('Erros de validacao encontrados');

			res.status(500).send(erros);
			return;
		}
		/* termino da verificação de erros */
		var cadastro = req.body;

		cadastro.status = "1";

		var connection = app.persistencia.connectionFactory();
		var contatoDao = new app.persistencia.ContatoDao(connection);

		contatoDao.novo(cadastro, function(erro, resultado){
			if (erro) {
				console.log('*** Erro ao cadastrar. ***');

				res.status(500).send(erro);
			} else {
				console.log('------------------');
				console.log('contato criado');
				console.log('------------------');

				res.location('/contact/' + resultado.insertId);
				res.status(201).json(resultado);

				return;
			}
		});
	});	

	//edita o contato (update)
	/*app.get('/contact/:id', function(req, res){
		console.log('==================');
		console.log("EXIBINDO DADOS DO CONTATO");
		console.log('==================');

		var cadastro = {};
		var id = req.params.id;

		cadastro.id = id;
		cadastro.status = "CONFIRMADO";

		var connection = app.persistencia.connectionFactory();
		var contatoDao = new app.persistencia.ContatoDao(connection);

		contatoDao.altera(cadastro, function(erro){
			if (erro) {
				res.status(500).send(erro);
				return;
			}
			res.send(cadastro);

		});
	});*/

	//"deleta" o contato
	app.delete('/contact/:id', function(req, res){
		console.log('==================');
		console.log("'REMOVENDO' CONTATO");
		console.log('==================');
		var contato = {};
		var id = req.params.id;

		contato.id = id;
		contato.status = "0";

		var connection = app.persistencia.connectionFactory();
		var contatoDao = new app.persistencia.ContatoDao(connection);

		contatoDao.remove(contato, function(erro){
			if (erro) {
				console.log("*** Contato não removido. Houve erro. ***");
				res.status(500).send(erro);
				return;
			} else {
				console.log('------------------');
				console.log("Contato removido(alterado status para 0)");
				console.log('------------------');
				res.status(410).send(contato);
				return;
			}
		});
	});

}
